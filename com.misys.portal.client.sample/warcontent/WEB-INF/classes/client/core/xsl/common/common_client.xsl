<?xml version="1.0" encoding="UTF-8"?>
<!--
##########################################################
Templates that are common across all XSLT stylesheets.

Copyright (c) 2000-2008 Misys (http://www.misys.com),
All Rights Reserved. 

version:   1.0
date:      12/03/08
author:    Cormac Flynn
email:     cormac.flynn@misys.com
##########################################################
-->
<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet 
		version="1.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:xd="http://www.pnp-software.com/XSLTdoc" 
		xmlns:localization="xalan://com.misys.portal.common.localization.Localization"
        xmlns:convertTools="xalan://com.misys.portal.common.tools.ConvertTools"
        xmlns:securityCheck="xalan://com.misys.portal.common.security.GTPSecurityCheck"
		xmlns:utils="xalan://com.misys.portal.common.tools.Utils"
		xmlns:security="xalan://com.misys.portal.security.GTPSecurity"
		xmlns:defaultresource="xalan://com.misys.portal.common.resources.DefaultResourceProvider"
		exclude-result-prefixes="localization convertTools securityCheck utils security defaultresource">

 <xsl:param name="contextPath"/>
 <xsl:param name="servletPath"/>
 <xsl:param name="option"></xsl:param>
 
<!--[COSTUMIZATION-SPECIFICATION] Test pour verifier si l'utilisateur a la permission simple_bg_types -->
 <xsl:template name="product_type_code_options">
 <xsl:choose>
 
  <xsl:when test="securityCheck:hasPermission($rundata,'simple_bg_types')">
		<xsl:for-each select="prod_type_code/product_type_details">
		<!--  Test des produits à afficher -->
			<xsl:if test="product_type_code = '03' or product_type_code = '04' or product_type_code = '05' or product_type_code = '08' or product_type_code = '10'">
	        <option>
	           <xsl:attribute name="value">
	            <xsl:value-of select="product_type_code"></xsl:value-of>
	            </xsl:attribute>
	            <xsl:value-of select="product_type_description"/>
	        </option>
	        </xsl:if>
      	</xsl:for-each>
      	</xsl:when>
      	<!-- Sinon -->
      	<xsl:otherwise>
      	<xsl:for-each select="prod_type_code/product_type_details">
	        <option>
	           <xsl:attribute name="value">
	            <xsl:value-of select="product_type_code"></xsl:value-of>
	            </xsl:attribute>
	            <xsl:value-of select="product_type_description"/>
	        </option>
      	</xsl:for-each>
      	</xsl:otherwise>
      	</xsl:choose>
	</xsl:template>

<!-- Character comitement : remove radio button  -->

 <xsl:template match="character_commitment">
   <xsl:param name="node-name"/>
   <xsl:param name="label"/>
<xsl:param name="conditional-label">XSL_GUARANTEE_CONDITIONAL</xsl:param>  
<xsl:param name="first-demand-label">XSL_GUARANTEE_FIRST_DEMAND</xsl:param>


  
<xsl:param name="first-value">01</xsl:param> <!--Value of the first radio button  --> 
<xsl:param name="second-value">02</xsl:param> <!--  Value of the second radio button -->
 
 
<!--[COSTUMIZATION-SPECIFICATION] Test pour verifier si l'utilisateur a la permission guarantee_first_demand -->  
   <!-- Can override labels -->
<!--  <xsl:if test="not(securityCheck:hasPermission($rundata,'guarantee_first_demand'))"> -->
<xsl:choose>
<!-- if the client has the  role " guarantee_first_demand" he will just have a label "first demand" -->
<xsl:when test="(securityCheck:hasPermission($rundata,'guarantee_first_demand'))">
<xsl:call-template name="multioption-inline-wrapper">
  
	      <xsl:with-param name="group-label" select="$label"/>
	      <xsl:with-param name="content">
	   

<!-- 		        <xsl:call-template name="multichoice-field"> -->
<!-- 			      <xsl:with-param name="group-label" select="$label"/> -->
<!-- 			      <xsl:with-param name="label" select="$conditional-label"/> -->
<!-- 			      <xsl:with-param name="name" select="$node-name"/> -->
<!-- 			      <xsl:with-param name="id"><xsl:value-of select="$node-name"/>_1</xsl:with-param> -->
<!-- 			      <xsl:with-param name="value" select="$first-value"/> -->
<!-- 			      <xsl:with-param name="checked"><xsl:if test="self::node()[. = $first-value]">Y</xsl:if></xsl:with-param> -->
<!-- 			      <xsl:with-param name="type">radiobutton</xsl:with-param> -->
<!-- 			      <xsl:with-param name="inline">Y</xsl:with-param> -->
<!-- 			     </xsl:call-template> -->
			
			     <xsl:call-template name="multichoice-field">
			      <xsl:with-param name="label" select="$first-demand-label"/>
			      <xsl:with-param name="name" select="$node-name"/>
<!-- 			      <xsl:with-param name="id"><xsl:value-of select="$node-name"/>_2</xsl:with-param> -->
<!-- 			      <xsl:with-param name="value" select="$second-value"/> -->
<!-- 			      <xsl:with-param name="checked">Y</xsl:with-param> -->
<!-- 			      <xsl:with-param name="type">label</xsl:with-param> -->
			      <xsl:with-param name="inline">Y</xsl:with-param>
			     </xsl:call-template>
 
			     
	    	</xsl:with-param>
	    	
   </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
   <!-- otherwise he will have 2 radios buttons -->
    <xsl:call-template name="multioption-inline-wrapper">
	      <xsl:with-param name="group-label" select="$label"/>
	      <xsl:with-param name="content">
		        <xsl:call-template name="multichoice-field">
			      <xsl:with-param name="group-label" select="$label"/>
			      <xsl:with-param name="label" select="$conditional-label"/>
			      <xsl:with-param name="name" select="$node-name"/>
			      <xsl:with-param name="id"><xsl:value-of select="$node-name"/>_1</xsl:with-param>
			      <xsl:with-param name="value" select="$first-value"/>
			      <xsl:with-param name="checked"><xsl:if test="self::node()[. = $first-value]">Y</xsl:if></xsl:with-param>
			      <xsl:with-param name="type">radiobutton</xsl:with-param>
			      <xsl:with-param name="inline">Y</xsl:with-param>
			     </xsl:call-template>
			    
			     <xsl:call-template name="multichoice-field">
			      <xsl:with-param name="label" select="$first-demand-label"/>
			      <xsl:with-param name="name" select="$node-name"/>
			      <xsl:with-param name="id"><xsl:value-of select="$node-name"/>_2</xsl:with-param>
			      <xsl:with-param name="value" select="$second-value"/>
			      <xsl:with-param name="checked"><xsl:if test="self::node()[. = $second-value]">Y</xsl:if></xsl:with-param>
			      <xsl:with-param name="type">radiobutton</xsl:with-param>
			      <xsl:with-param name="inline">Y</xsl:with-param>
			     </xsl:call-template>
	    	</xsl:with-param>
    </xsl:call-template>
   </xsl:otherwise>
   </xsl:choose>
   
    
<!--     </xsl:if> -->
 </xsl:template>
 
 
<!--  for BG rules dropdown input -->
	<xsl:template name="product_rule_options">
		<xsl:choose>
		<!--[COSTUMIZATION-SPECIFICATION] Test pour verifier si l'utilisateur a la permission simple_bg_rules-->  
 		<!-- Test pour verifier si l'utilisateur a la permission en question -->
 			 <xsl:when test="securityCheck:hasPermission($rundata,'simple_bg_rules')">
				<xsl:for-each select="prod_rule/product_rule_details">
					<xsl:if test="product_rule_code = '03' or product_rule_code = '04' or product_rule_code = '06' or product_rule_code = '07' or product_rule_code = '08' or product_rule_code = '09' or product_rule_code = '99'">
	        			<option>
	           				<xsl:attribute name="value">
	            				<xsl:value-of select="product_rule_code"></xsl:value-of>
	            			</xsl:attribute>
	            			<xsl:value-of select="product_rule_description"/>
	        			</option>
	        		</xsl:if>
      			</xsl:for-each>
      		</xsl:when>
      		<!-- Sinon -->
      		<xsl:otherwise>
      			<xsl:for-each select="prod_rule/product_rule_details">
	        		<option>
	           			<xsl:attribute name="value">
	            			<xsl:value-of select="product_rule_code"></xsl:value-of>
	            		</xsl:attribute>
	            		<xsl:value-of select="product_rule_description"/>
	        		</option>
      			</xsl:for-each>
      		</xsl:otherwise>
      	</xsl:choose>
	</xsl:template>
   
    <!--
   BG Guarantee Details 
   -->
     <!-- to disable checkbox Provisoire 
   will be disabled in the BG we add this template -->
  <xsl:template name="bg-guarantee-details">
  	 <xsl:param name="pdfOption"/>
  	 <xsl:param name="isBankReporting"/>
  	 <xsl:variable name="displayProvisionalCheckBox">
			<xsl:choose>
				<xsl:when test="$isBankReporting='Y'">N</xsl:when>
				<xsl:otherwise>Y</xsl:otherwise>
			</xsl:choose>
	</xsl:variable>
   <xsl:call-template name="fieldset-wrapper">
    <xsl:with-param name="legend">XSL_HEADER_GTEE_DETAILS</xsl:with-param>
    <xsl:with-param name="content">
    <xsl:if test="$displaymode='edit'">
	      <xsl:call-template name="select-field">
		      <xsl:with-param name="label">XSL_GTEEDETAILS_TYPE_LABEL</xsl:with-param>
		      <xsl:with-param name="name">bg_type_code</xsl:with-param>
		      <xsl:with-param name="required">Y</xsl:with-param>
		      <xsl:with-param name="readonly"><xsl:if test="bg_code[.!='']">Y</xsl:if></xsl:with-param>
		      <xsl:with-param name="value"><xsl:if test="bg_type_code[.!='']"><xsl:value-of select="bg_type_code"/></xsl:if></xsl:with-param>
		      <xsl:with-param name="options">
		       <xsl:call-template name="product_type_code_options"/>
		      </xsl:with-param>
	      </xsl:call-template>
      </xsl:if>
      <xsl:if test="$displaymode='view'">
      			<xsl:variable name="gtee_type_code"><xsl:value-of select="bg_type_code"></xsl:value-of></xsl:variable>
				<xsl:variable name="productCode"><xsl:value-of select="product_code"/></xsl:variable>
				<xsl:variable name="subProductCode"><xsl:value-of select="sub_product_code"/></xsl:variable>
				<xsl:variable name="parameterId">C011</xsl:variable>
      		<xsl:call-template name="input-field">
		      <xsl:with-param name="label">XSL_GTEEDETAILS_TYPE_LABEL</xsl:with-param>
		      <xsl:with-param name="name">bg_type_code</xsl:with-param>
		      <xsl:with-param name="required">Y</xsl:with-param>
		      <xsl:with-param name="readonly"><xsl:if test="bg_code[.!='']">Y</xsl:if></xsl:with-param>
		      <xsl:with-param name="value"><xsl:if test="bg_type_code[.!='']"><xsl:value-of select="localization:getCodeData($language,'*',$productCode,$parameterId, $gtee_type_code)"/></xsl:if></xsl:with-param>
	      </xsl:call-template>
      </xsl:if>
     <xsl:if test="$displayProvisionalCheckBox='Y'">
        <xsl:choose>
        <!--[COSTUMIZATION-SPECIFICATION] Test pour verifier si l'utilisateur a la permission bg_provisional_mandatory-->  
  		 <!-- Ajout de la permission pour la case à cocher Provisoire BG-->
  		 <xsl:when test="securityCheck:hasPermission($rundata,'bg_provisional_mandatory')">
		     <div id="pro-check-box">
			     <xsl:call-template name="checkbox-field">
				     <xsl:with-param name="label">XSL_PROVISIONAL</xsl:with-param>
				     <xsl:with-param name="name">provisional_status</xsl:with-param>
				     <xsl:with-param name="checked">Y</xsl:with-param>
				     <xsl:with-param name="disabled">Y</xsl:with-param>
				 </xsl:call-template>
			 </div>
		 </xsl:when>
		 <xsl:otherwise>
			 <div id="pro-check-box">
			     <xsl:call-template name="checkbox-field">
				     <xsl:with-param name="label">XSL_PROVISIONAL</xsl:with-param>
				     <xsl:with-param name="name">provisional_status</xsl:with-param>
				 </xsl:call-template>
			 </div>
		 </xsl:otherwise>
		 </xsl:choose>
	 </xsl:if>
     <div id="bgtypedetails-editor">
	     <xsl:call-template name="input-field">
	      <xsl:with-param name="id">bg_type_details</xsl:with-param>
	      <xsl:with-param name="name">bg_type_details</xsl:with-param>
	      <xsl:with-param name="maxsize">40</xsl:with-param>
	      <!-- <xsl:with-param name="required">Y</xsl:with-param> -->
	     </xsl:call-template>
     </div>
     <!-- <xsl:call-template name="input-field">
      <xsl:with-param name="name">bg_type_details</xsl:with-param>
      <xsl:with-param name="maxsize">255</xsl:with-param>
      <xsl:with-param name="readonly">Y</xsl:with-param>
     </xsl:call-template>-->
     <xsl:if test="bg_code[.!='']">
     <xsl:call-template name="input-field">
      <xsl:with-param name="label">XSL_GUARANTEE_NAME</xsl:with-param>
      <xsl:with-param name="name">bg_code</xsl:with-param>
      <xsl:with-param name="maxsize">40</xsl:with-param>
      <xsl:with-param name="readonly">Y</xsl:with-param>
      <xsl:with-param name="required">Y</xsl:with-param>
     </xsl:call-template>
     </xsl:if>

	<!-- BG Text type as hidden field -->
	<xsl:call-template name="hidden-field">
		<xsl:with-param name="name">bg_text_details_code</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="hidden-field">
		<xsl:with-param name="name">guarantee_type_code</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="hidden-field">
		<xsl:with-param name="name">guarantee_type_company_id</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="hidden-field">
		<xsl:with-param name="name">guarantee_type_name</xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="hidden-field">
		<xsl:with-param name="name">speciman</xsl:with-param>
	</xsl:call-template>
		<xsl:if test="bg_text_details_code = '01' and $isBankReporting!='Y'">
	     <xsl:choose>
	     <xsl:when test="speciman != ''" >
			<xsl:call-template name="row-wrapper">
				<xsl:with-param name="id">display_specimen</xsl:with-param>
				<xsl:with-param name="content">
			       (<a>
			         <xsl:attribute name="href">javascript:void(0)</xsl:attribute>
			         <xsl:attribute name="onclick">misys.downloadStaticDocument('document_id');</xsl:attribute>
			         <xsl:attribute name="title"><xsl:value-of select="localization:getGTPString($language, 'ACTION_DISPLAY_SPECIMEN')"/></xsl:attribute>
			         <xsl:value-of select="localization:getGTPString($language, 'ACTION_DISPLAY_SPECIMEN')"/>
			        </a>)
					<xsl:call-template name="hidden-field">
						<xsl:with-param name="name">document_id</xsl:with-param>
					</xsl:call-template>
	        	</xsl:with-param>
	        </xsl:call-template>
	     </xsl:when>
	     <xsl:otherwise>
		     <xsl:if test="$displaymode='edit'" >
		     	<xsl:call-template name="row-wrapper">
			      <xsl:with-param name="id">GTEETextPreview</xsl:with-param>
			      <xsl:with-param name="label"></xsl:with-param>
			      <xsl:with-param name="content">
			       <a name="GTEETextPreview" href="javascript:void(0)" onclick="misys.generateGTEEFromNew();return false;">
						<xsl:value-of select="localization:getGTPString($language, 'XSL_GTEEDETAILS_TEXT_PREVIEW_LABEL')"/>
				   </a>
			      </xsl:with-param>
			     </xsl:call-template>
		     </xsl:if>
	     </xsl:otherwise>
	     </xsl:choose>
	     </xsl:if>
	     <xsl:if test="bg_text_details_code = '02' and $isBankReporting!='Y'">
	     	<xsl:call-template name="row-wrapper">
				<xsl:with-param name="id">display_specimen</xsl:with-param>
				<xsl:with-param name="content">
					<xsl:variable name="refId"><xsl:if test="$option != 'SCRATCH'"><xsl:value-of select="ref_id"/></xsl:if></xsl:variable>
					<xsl:variable name="tnxId"><xsl:if test="$option != 'SCRATCH'"><xsl:value-of select="tnx_id"/></xsl:if></xsl:variable>
								
			       (<a>
			         <xsl:attribute name="href">javascript:void(0)</xsl:attribute>
			         <xsl:attribute name="onclick">javascript:misys.popup.generateDocument('bg-document', '<xsl:value-of select="$pdfOption"/>', '<xsl:value-of select="$refId"/>', '<xsl:value-of select="$tnxId"/>', '<xsl:value-of select="product_code"/>', '<xsl:value-of select="bg_code"/>','<xsl:value-of select="guarantee_type_company_id"/>');</xsl:attribute>
			         <xsl:attribute name="title"><xsl:value-of select="localization:getGTPString($language, 'XSL_GTEEDETAILS_TEXT_TYPE_VIEW_EDITED_DOCUMENT')"/></xsl:attribute>
			         <xsl:value-of select="localization:getGTPString($language, 'XSL_GTEEDETAILS_TEXT_TYPE_VIEW_EDITED_DOCUMENT')"/>
			        </a>)
	        	</xsl:with-param>
	        </xsl:call-template>
	     </xsl:if>
     
     <xsl:apply-templates select="character_commitment">
        <xsl:with-param name="node-name">character_commitment</xsl:with-param>
        <xsl:with-param name="label">XSL_GUARANTEE_CHARACTER_COMMITMENT_LABEL</xsl:with-param>
     </xsl:apply-templates>
     <xsl:if test="$displaymode='edit'">
	     <xsl:call-template name="select-field">
	      <xsl:with-param name="label">XSL_GTEEDETAILS_RULES_LABEL</xsl:with-param>
	      <xsl:with-param name="name">bg_rule</xsl:with-param>
	      <xsl:with-param name="options">
	       <xsl:call-template name="product_rule_options"/>
	      </xsl:with-param>
	     </xsl:call-template>
     </xsl:if>
     <xsl:if test="$displaymode='view'">
	    <xsl:variable name="bg_rule_code"><xsl:value-of select="bg_rule"></xsl:value-of></xsl:variable>
		<xsl:variable name="productCode"><xsl:value-of select="product_code"/></xsl:variable>
		<xsl:variable name="subProductCode"><xsl:value-of select="sub_product_code"/></xsl:variable>
		<xsl:variable name="parameterId">C013</xsl:variable>
		<xsl:call-template name="input-field">
		 	<xsl:with-param name="label">XSL_GTEEDETAILS_RULES_LABEL</xsl:with-param>
		 	<xsl:with-param name="name">bg_rule</xsl:with-param>
		 	<xsl:with-param name="value"><xsl:value-of select="localization:getCodeData($language,'*',$productCode,$parameterId, $bg_rule_code)"/></xsl:with-param>
		 	<xsl:with-param name="override-displaymode">view</xsl:with-param>	
		</xsl:call-template>
     </xsl:if>
     <xsl:call-template name="input-field">
	   <xsl:with-param name="name">bg_rule_other</xsl:with-param>
	   <xsl:with-param name="maxsize">35</xsl:with-param>
	   <xsl:with-param name="readonly">Y</xsl:with-param>
	   <xsl:with-param name="required">Y</xsl:with-param>
	 </xsl:call-template>
     
     <!-- <xsl:if test="bg_text_details_code = '01'"> -->
	   <!-- MPS-21209 : BG Initiation - Guarantee details - 'Delivery To' field missing in Review screen  -->
	   <xsl:if test="$displaymode='view'">
     	<xsl:call-template name="select-field">
	      <xsl:with-param name="label">XSL_GTEEDETAILS_DELIVERY_TO_LABEL</xsl:with-param>
	      <xsl:with-param name="name">delivery_to</xsl:with-param>
	      <xsl:with-param name="options">
	       <xsl:call-template name="bg-delivery-to"/>
	      </xsl:with-param>
	     </xsl:call-template>
    	</xsl:if>
	   	<!-- End : MPS-21209 -->     
	     <xsl:call-template name="select-field">
	      <xsl:with-param name="label">XSL_GTEEDETAILS_TEXT_TYPE_LABEL</xsl:with-param>
	      <xsl:with-param name="name">bg_text_type_code</xsl:with-param>
	      <xsl:with-param name="required">Y</xsl:with-param>
	      <xsl:with-param name="options">
	       <xsl:call-template name="bg-guarantee-text">
	       		<xsl:with-param name="pdfOption"><xsl:value-of select="$pdfOption"/></xsl:with-param>
	       </xsl:call-template>
	      </xsl:with-param>
	     </xsl:call-template>
	     <xsl:call-template name="input-field">
	      <xsl:with-param name="name">bg_text_type_details</xsl:with-param>
	      <xsl:with-param name="maxsize">255</xsl:with-param>
	      <xsl:with-param name="required">Y</xsl:with-param>
	     </xsl:call-template>
	     <!-- <div id="document-editor" style="display:none;"><br/>
	      <xsl:call-template name="richtextarea-field">
	       <xsl:with-param name="label">XSL_REPORT_BG</xsl:with-param>
	       <xsl:with-param name="name">bg_document</xsl:with-param>
	       <xsl:with-param name="rows">13</xsl:with-param>
	       <xsl:with-param name="cols">40</xsl:with-param>
	       <xsl:with-param name="instantiation-event">/document-editor/display</xsl:with-param>	
	      </xsl:call-template>
	     </div> -->
	     <xsl:call-template name="select-field">
	      <xsl:with-param name="label">XSL_GTEEDETAILS_TEXT_LANGUAGE</xsl:with-param>
	      <xsl:with-param name="name">text_language</xsl:with-param>
	      <xsl:with-param name="options">
	       <xsl:call-template name="bg-text-languages"/>
	      </xsl:with-param>
	     </xsl:call-template>
	     <xsl:call-template name="input-field">
	      <xsl:with-param name="name">text_language_other</xsl:with-param>
	      <xsl:with-param name="maxsize">35</xsl:with-param>
	      <xsl:with-param name="readonly">Y</xsl:with-param>
	      <xsl:with-param name="required">Y</xsl:with-param>
	     </xsl:call-template>
     <!-- </xsl:if> -->
     <xsl:choose>
	     <xsl:when test="$displaymode='edit'">
		     <xsl:call-template name="row-wrapper">
		      <xsl:with-param name="id">narrative_additional_instructions</xsl:with-param>
		      <xsl:with-param name="label">XSL_GTEEDETAILS_OTHER_INSTRUCTIONS</xsl:with-param>
		      <xsl:with-param name="type">textarea</xsl:with-param>
		      <xsl:with-param name="content">
		       <xsl:call-template name="textarea-field">
		        <xsl:with-param name="name">narrative_additional_instructions</xsl:with-param>
		        <xsl:with-param name="rows">13</xsl:with-param>
		        <xsl:with-param name="cols">40</xsl:with-param>
		       </xsl:call-template>
		      </xsl:with-param>
		     </xsl:call-template>
	     </xsl:when>
	     <xsl:when test="$displaymode='view' and narrative_additional_instructions[.!='']">
	     	<xsl:call-template name="big-textarea-wrapper">
		      <xsl:with-param name="label">XSL_GTEEDETAILS_OTHER_INSTRUCTIONS</xsl:with-param>
		      <xsl:with-param name="content"><div class="content">
		        <xsl:value-of select="narrative_additional_instructions"/>
		      </div></xsl:with-param>
		     </xsl:call-template>
	     </xsl:when>
     </xsl:choose>
     <xsl:if test="$displaymode='view'">
	     <xsl:call-template name="hidden-field">
		    <xsl:with-param name="name">bg_text_type_code</xsl:with-param>
		    <xsl:with-param name="value"><xsl:value-of select="bg_text_type_code"/></xsl:with-param>
		 </xsl:call-template>
	 </xsl:if>
    </xsl:with-param>
   </xsl:call-template>
  </xsl:template>
 
 
  <!-- to disable checkbox Provisoire 
   will be disabled in the standby-lc we add this template -->
  
  <xsl:template name="standby-lc-details">
  	<xsl:param name="featureId"/>
  	<xsl:param name="isBank"/>
  	<xsl:param name="isAmend">N</xsl:param>
  	<xsl:param name="isStructuredFormat">N</xsl:param>
  	
  	<xsl:call-template name="fieldset-wrapper">
	    <xsl:with-param name="legend">XSL_HEADER_STANDBY_LC_DETAILS</xsl:with-param>
	    <xsl:with-param name="content">
	    <xsl:call-template name="hidden-field">
			<xsl:with-param name="name">standby_text_type_code</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="hidden-field">
			<xsl:with-param name="name">standby_template_bank_id</xsl:with-param>
		</xsl:call-template>
	  	<xsl:choose>
	    	<xsl:when test="$displaymode='edit' ">
		    	<xsl:if test= "product_type_code ='' and prod_type_code/product_type_details/product_type_description !='' and $isStructuredFormat != 'Y'">
					<xsl:call-template name="select-field">
			    		<xsl:with-param name="label">XSL_SBLCDETAILS_TYPE_LABEL</xsl:with-param>
			    		<xsl:with-param name="name">product_type_code</xsl:with-param>
			    		<xsl:with-param name="required">Y</xsl:with-param>
			    		<xsl:with-param name="value"></xsl:with-param>
			    		<xsl:with-param name="options">
					      	<xsl:call-template name="product_type_code_options"/>
					    </xsl:with-param>
			    	</xsl:call-template>
				</xsl:if>
				<xsl:if test= "product_type_code ='' and prod_type_code/product_type_details/product_type_description !='' and $isStructuredFormat = 'Y'">
					<xsl:call-template name="select-field">
			    		<xsl:with-param name="label">XSL_SBLCDETAILS_TYPE_LABEL</xsl:with-param>
			    		<xsl:with-param name="name">product_type_code</xsl:with-param>
			    		<xsl:with-param name="required">Y</xsl:with-param>
			    		<xsl:with-param name="disabled">N</xsl:with-param>	
			    		<xsl:with-param name="value"></xsl:with-param>
			    		<xsl:with-param name="options">
					      	<xsl:call-template name="product_type_code_options"/>
					    </xsl:with-param>
			    	</xsl:call-template>
				</xsl:if>
				<xsl:if test="product_type_code[.!=''] and prod_type_code/product_type_details/product_type_description[.!=''] and $isAmend!='Y' and $isBank !='Y'">
		    		<xsl:call-template name="select-field">
			    		<xsl:with-param name="label">XSL_SBLCDETAILS_TYPE_LABEL</xsl:with-param>
			    		<xsl:with-param name="name">product_type_code</xsl:with-param>
			    		<xsl:with-param name="required">Y</xsl:with-param>
			    		<xsl:with-param name="value"><xsl:value-of select="product_type_code"/></xsl:with-param>
			    		<xsl:with-param name="options">
					      	<xsl:call-template name="product_type_code_options"/>
					    </xsl:with-param>
			    	</xsl:call-template>
			    </xsl:if>
			    <xsl:if test="product_type_code[.!=''] and prod_type_code/product_type_details/product_type_description[.!=''] and $isAmend='Y'">
		    		<xsl:call-template name="select-field">
			    		<xsl:with-param name="label">XSL_SBLCDETAILS_TYPE_LABEL</xsl:with-param>
			    		<xsl:with-param name="name">product_type_code</xsl:with-param>
			    		<xsl:with-param name="disabled">Y</xsl:with-param>
			    		<xsl:with-param name="required">Y</xsl:with-param>
			    		<xsl:with-param name="value"><xsl:value-of select="product_type_code"/></xsl:with-param>
			    		<xsl:with-param name="options">
					      	<xsl:call-template name="product_type_code_options"/>
					    </xsl:with-param>
			    	</xsl:call-template>
			    </xsl:if>
			    <xsl:if test="product_type_code !='' and $isBank='Y'">
					<xsl:variable name="sblc_type_code"><xsl:value-of select="product_type_code"></xsl:value-of></xsl:variable>
					<xsl:variable name="productCode"><xsl:value-of select="product_code"/></xsl:variable>
					<xsl:variable name="subProductCode"><xsl:value-of select="sub_product_code"/></xsl:variable>
					<xsl:variable name="parameterId">C010</xsl:variable>
					<xsl:call-template name="select-field">
					 	<xsl:with-param name="label">XSL_SBLCDETAILS_TYPE_LABEL</xsl:with-param>
					 	<xsl:with-param name="name">product_type_code</xsl:with-param>
					 	<xsl:with-param name="value"><xsl:value-of select="product_type_code"/></xsl:with-param>
					 	<xsl:with-param name="options">
						 	<option>
					           <xsl:attribute name="value">
					            <xsl:value-of select="product_type_code"></xsl:value-of>
					            </xsl:attribute>
					            <xsl:value-of select="localization:getCodeData($language,'*',$productCode,$parameterId, $sblc_type_code)"/>
					        </option>
				        </xsl:with-param>
					 	<xsl:with-param name="disabled">
					 		<xsl:choose>
					 			<xsl:when test="$isStructuredFormat = 'Y'">N</xsl:when>
					 			<xsl:otherwise>Y</xsl:otherwise>
					 		</xsl:choose>
					 	</xsl:with-param>	
					 	<xsl:with-param name="required">Y</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			    <xsl:if test="product_type_details[.!='']">
					<xsl:call-template name="input-field">
					 	<xsl:with-param name="label"></xsl:with-param>
					 	<xsl:with-param name="name">product_type_details</xsl:with-param>
					 	<xsl:with-param name="value"><xsl:value-of select="product_type_details"/></xsl:with-param>	
					</xsl:call-template>
				</xsl:if>
<!-- 				<xsl:if test="$isBank!='Y'"> -->
				
<xsl:choose>

   <!-- add permission to the checkbox Provisoire -->
   <!-- we test if customer user has the permission si_provisional_mandatory the checkbox Provisoire 
   will be disabled -->
      <!--[COSTUMIZATION-SPECIFICATION] Test pour verifier si l'utilisateur a la permission si_provisional_mandatory-->
   
   <xsl:when test="($isBank!='Y' and securityCheck:hasPermission($rundata,'si_provisional_mandatory'))">
	     
				    <div id="pro-check-box">
						 <xsl:call-template name="checkbox-field">
							 <xsl:with-param name="label">XSL_PROVISIONAL</xsl:with-param>
							 <xsl:with-param name="name">provisional_status</xsl:with-param>
							      <xsl:with-param name="disabled">Y</xsl:with-param>
						 </xsl:call-template>
					 </div>
					  </xsl:when> 
   <xsl:otherwise>
   
	  <xsl:if test="$isBank!='Y'">
	   <div id="pro-check-box">
						 <xsl:call-template name="checkbox-field">
							 <xsl:with-param name="label">XSL_PROVISIONAL</xsl:with-param>
							 <xsl:with-param name="name">provisional_status</xsl:with-param>
<!-- 							      <xsl:with-param name="disabled">Y</xsl:with-param> -->
						 </xsl:call-template>
					 </div>
		</xsl:if>
		
   </xsl:otherwise>
   </xsl:choose>
					 
					 
					 
<!-- 				 </xsl:if> -->
				<xsl:if test="(product_type_details ='' and $isBank != 'Y') or (product_type_details ='' and $isBank = 'Y' and $isStructuredFormat = 'Y')">
					<xsl:call-template name="input-field">
					 	<xsl:with-param name="label"></xsl:with-param>
					 	<xsl:with-param name="name">product_type_details</xsl:with-param>
					 	<xsl:with-param name="value"></xsl:with-param>	
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="$featureId!='' and stand_by_lc_code[.='']">
			    	<xsl:call-template name="input-field">
			    		<xsl:with-param name="label">XSL_SBLC_NAME</xsl:with-param>
			    		<xsl:with-param name="name">stand_by_lc_code</xsl:with-param>
			    		<xsl:with-param name="required">Y</xsl:with-param>
			    		<xsl:with-param name="value"><xsl:value-of select="$featureId"></xsl:value-of></xsl:with-param>
			    		<xsl:with-param name="disabled">Y</xsl:with-param>
			    	</xsl:call-template> 
			    </xsl:if>
			    <xsl:if test="stand_by_lc_code[.!='']">
			    	<xsl:call-template name="input-field">
			    		<xsl:with-param name="label">XSL_SBLC_NAME</xsl:with-param>
			    		<xsl:with-param name="name">stand_by_lc_code</xsl:with-param>
			    		<xsl:with-param name="required">Y</xsl:with-param>
			    		<xsl:with-param name="value"><xsl:value-of select="stand_by_lc_code"></xsl:value-of></xsl:with-param>
			    		<xsl:with-param name="disabled">Y</xsl:with-param>
			    	</xsl:call-template> 
			    </xsl:if>
			    
			    <xsl:if test="standby_text_type_code = '01' and $isBank!='Y'">
			    <xsl:choose>
			    <xsl:when test="speciman != ''">
					<xsl:call-template name="row-wrapper">
						<xsl:with-param name="id">display_specimen</xsl:with-param>
						<xsl:with-param name="content">
					       (<a>
					         <xsl:attribute name="href">javascript:void(0)</xsl:attribute>
					         <xsl:attribute name="onclick">misys.downloadStaticDocument('document_id');</xsl:attribute>
					         <xsl:attribute name="title"><xsl:value-of select="localization:getGTPString($language, 'ACTION_DISPLAY_STAND_BY_LC_SPECIMEN')"/></xsl:attribute>
					         <xsl:value-of select="localization:getGTPString($language, 'ACTION_DISPLAY_STAND_BY_LC_SPECIMEN')"/>
					        </a>)
							<xsl:call-template name="hidden-field">
								<xsl:with-param name="name">document_id</xsl:with-param>
							</xsl:call-template>
			        	</xsl:with-param>
			        </xsl:call-template>
			     </xsl:when>
			     <xsl:otherwise>
			     	<xsl:call-template name="row-wrapper">
		      		<xsl:with-param name="id">GTEETextPreview</xsl:with-param>
		      		<xsl:with-param name="label"></xsl:with-param>
		      		<xsl:with-param name="content">
		      		<a name="GTEETextPreview" href="javascript:void(0)" onclick="misys.generateGTEEFromNew();return false;">
					<xsl:value-of select="localization:getGTPString($language, 'XSL_SBLCDETAILS_TEXT_PREVIEW_LABEL')"/>
			   		</a>
		     	 	</xsl:with-param>
		     		</xsl:call-template>
			     </xsl:otherwise>	
			     </xsl:choose>
			     </xsl:if>
			     <xsl:if test="standby_text_type_code = '02' and $isBank!='Y'">
			     	<xsl:variable name="pdfOption">
			     		<xsl:choose>
						   <xsl:when test="security:isBank($rundata)">PDF_SI_DOCUMENT_DETAILS_BANK</xsl:when>
						   <xsl:otherwise>PDF_SI_DOCUMENT_DETAILS</xsl:otherwise>
						 </xsl:choose>
			     	</xsl:variable>
			     	<xsl:call-template name="row-wrapper">
						<xsl:with-param name="id">display_specimen</xsl:with-param>
						<xsl:with-param name="content">
							<xsl:variable name="refId"><xsl:if test="$option != 'SCRATCH'"><xsl:value-of select="ref_id"/></xsl:if></xsl:variable>
							<xsl:variable name="tnxId"><xsl:if test="$option != 'SCRATCH'"><xsl:value-of select="tnx_id"/></xsl:if></xsl:variable>
										
					       (<a>
					         <xsl:attribute name="href">javascript:void(0)</xsl:attribute>
					         <xsl:attribute name="onclick">javascript:misys.popup.generateDocument('si-document', '<xsl:value-of select="$pdfOption"/>', '<xsl:value-of select="$refId"/>', '<xsl:value-of select="$tnxId"/>', '<xsl:value-of select="product_code"/>', '<xsl:value-of select="stand_by_lc_code"/>','<xsl:value-of select="standby_template_bank_id"/>');</xsl:attribute>
					         <xsl:attribute name="title"><xsl:value-of select="localization:getGTPString($language, 'XSL_GTEEDETAILS_TEXT_TYPE_VIEW_EDITED_DOCUMENT')"/></xsl:attribute>
					         <xsl:value-of select="localization:getGTPString($language, 'XSL_GTEEDETAILS_TEXT_TYPE_VIEW_EDITED_DOCUMENT')"/>
					        </a>)
			        	</xsl:with-param>
			        </xsl:call-template>
			     </xsl:if>
			     <xsl:if test="standby_rule_code[.=''] and prod_rule/product_rule_details/product_rule_description[.!='']">
				     <xsl:call-template name="select-field">
			    		<xsl:with-param name="label">XSL_GTEEDETAILS_RULES_LABEL</xsl:with-param>
			    		<xsl:with-param name="name">standby_rule_code</xsl:with-param>
			    		<xsl:with-param name="value"></xsl:with-param>
			    		<xsl:with-param name="options">
					      	<xsl:call-template name="product_rule_options"/>
					    </xsl:with-param>
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="standby_rule_code[.!=''] and $isAmend='Y'">
				     <xsl:call-template name="select-field">
			    		<xsl:with-param name="label">XSL_GTEEDETAILS_RULES_LABEL</xsl:with-param>
			    		<xsl:with-param name="name">standby_rule_code</xsl:with-param>
			    		<xsl:with-param name="value"><xsl:value-of select="standby_rule_code"/></xsl:with-param>
			    		<xsl:with-param name="options">
					      	<xsl:call-template name="product_rule_options"/>
					    </xsl:with-param>
					    <xsl:with-param name="disabled">Y</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			     <xsl:if test="standby_rule_code[.!=''] and $isAmend!='Y'">
				     <xsl:call-template name="select-field">
			    		<xsl:with-param name="label">XSL_GTEEDETAILS_RULES_LABEL</xsl:with-param>
			    		<xsl:with-param name="name">standby_rule_code</xsl:with-param>
			    		<xsl:with-param name="value"><xsl:value-of select="standby_rule_code"/></xsl:with-param>
			    		<xsl:with-param name="options">
					      	<xsl:call-template name="product_rule_options"/>
					    </xsl:with-param>
					</xsl:call-template>
				</xsl:if>
				<xsl:call-template name="input-field">
				   <xsl:with-param name="name">standby_rule_other</xsl:with-param>
				   <xsl:with-param name="maxsize">35</xsl:with-param>
				   <xsl:with-param name="readonly">Y</xsl:with-param>
				   <xsl:with-param name="required">Y</xsl:with-param>
				</xsl:call-template>
	    	</xsl:when>
	    	
	    	<xsl:otherwise >
	    		<xsl:if test="product_type_code[.!='']">
				<xsl:variable name="sblc_type_code"><xsl:value-of select="product_type_code"></xsl:value-of></xsl:variable>
				<xsl:variable name="productCode"><xsl:value-of select="product_code"/></xsl:variable>
				<xsl:variable name="subProductCode"><xsl:value-of select="sub_product_code"/></xsl:variable>
				<xsl:variable name="parameterId">C010</xsl:variable>
					<xsl:call-template name="input-field">
					 	<xsl:with-param name="label">XSL_SBLCDETAILS_TYPE_LABEL</xsl:with-param>
					 	<xsl:with-param name="name">product_type_code</xsl:with-param>
					 	<xsl:with-param name="value"><xsl:value-of select="localization:getCodeData($language,'*',$productCode,$parameterId, $sblc_type_code)"/></xsl:with-param>
					 	<xsl:with-param name="override-displaymode">view</xsl:with-param>	
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="$isBank!='Y'">
				    <div id="pro-check-box">
						 <xsl:call-template name="checkbox-field">
							 <xsl:with-param name="label">XSL_PROVISIONAL</xsl:with-param>
							 <xsl:with-param name="name">provisional_status</xsl:with-param>
							 <xsl:with-param name="override-displaymode">view</xsl:with-param>
<!-- 							      <xsl:with-param name="disabled">Y</xsl:with-param> -->
						 </xsl:call-template>
					 </div>
				 </xsl:if>
				<xsl:if test="product_type_details[.!='']">
					<xsl:call-template name="input-field">
					 	<xsl:with-param name="label"></xsl:with-param>
					 	<xsl:with-param name="name">product_type_details</xsl:with-param>
					 	<xsl:with-param name="value"><xsl:value-of select="product_type_details"/></xsl:with-param>
					 	<xsl:with-param name="override-displaymode">view</xsl:with-param>	
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="stand_by_lc_code[.!='']">
					<xsl:call-template name="input-field">
					 	<xsl:with-param name="label">XSL_SBLC_NAME</xsl:with-param>
					 	<xsl:with-param name="name">stand_by_lc_code</xsl:with-param>
					 	<xsl:with-param name="value"><xsl:value-of select="stand_by_lc_code"/></xsl:with-param>
					 	<xsl:with-param name="override-displaymode">view</xsl:with-param>	
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="standby_rule_code[.!='']">
					<xsl:variable name="sblc_rule_code"><xsl:value-of select="standby_rule_code"/></xsl:variable>
					<xsl:variable name="productCode"><xsl:value-of select="product_code"/></xsl:variable>
					<xsl:variable name="subProductCode"><xsl:value-of select="sub_product_code"/></xsl:variable>
					<xsl:variable name="parameterId">C012</xsl:variable>
					<xsl:call-template name="input-field">
			    		<xsl:with-param name="label">XSL_GTEEDETAILS_RULES_LABEL</xsl:with-param>
			    		<xsl:with-param name="name">standby_rule_code</xsl:with-param>
			    		<xsl:with-param name="value"><xsl:value-of select="localization:getCodeData($language,'*',$productCode,$parameterId, $sblc_rule_code)"/></xsl:with-param>
			    		<xsl:with-param name="override-displaymode">view</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="standby_text_type_code = '01' and $isBank!='Y'"> 
				    <xsl:if test="speciman != ''">
						<xsl:call-template name="row-wrapper">
							<xsl:with-param name="id">display_specimen</xsl:with-param>
							<xsl:with-param name="content">
						       (<a>
						         <xsl:attribute name="href">javascript:void(0)</xsl:attribute>
						         <xsl:attribute name="onclick">misys.downloadStaticDocument('document_id');</xsl:attribute>
						         <xsl:attribute name="title"><xsl:value-of select="localization:getGTPString($language, 'ACTION_DISPLAY_STAND_BY_LC_SPECIMEN')"/></xsl:attribute>
						         <xsl:value-of select="localization:getGTPString($language, 'ACTION_DISPLAY_STAND_BY_LC_SPECIMEN')"/>
						        </a>)
								<xsl:call-template name="hidden-field">
									<xsl:with-param name="name">document_id</xsl:with-param>
								</xsl:call-template>
				        	</xsl:with-param>
				        </xsl:call-template>
				     </xsl:if>	
			     </xsl:if>
				<xsl:if test="standby_text_type_code = '02' and $isBank!='Y'">
			     	<xsl:variable name="pdfOption">
			     		<xsl:choose>
						   <xsl:when test="security:isBank($rundata)">PDF_SI_DOCUMENT_DETAILS_BANK</xsl:when>
						   <xsl:otherwise>PDF_SI_DOCUMENT_DETAILS</xsl:otherwise>
						 </xsl:choose>
			     	</xsl:variable>
			     	<xsl:call-template name="row-wrapper">
						<xsl:with-param name="id">display_specimen</xsl:with-param>
						<xsl:with-param name="content">
							<xsl:variable name="refId"><xsl:if test="$option != 'SCRATCH'"><xsl:value-of select="ref_id"/></xsl:if></xsl:variable>
							<xsl:variable name="tnxId"><xsl:if test="$option != 'SCRATCH'"><xsl:value-of select="tnx_id"/></xsl:if></xsl:variable>
										
					       (<a>
					         <xsl:attribute name="href">javascript:void(0)</xsl:attribute>
					         <xsl:attribute name="onclick">javascript:misys.popup.generateDocument('si-document', '<xsl:value-of select="$pdfOption"/>', '<xsl:value-of select="$refId"/>', '<xsl:value-of select="$tnxId"/>', '<xsl:value-of select="product_code"/>', '<xsl:value-of select="stand_by_lc_code"/>','<xsl:value-of select="standby_template_bank_id"/>');</xsl:attribute>
					         <xsl:attribute name="title"><xsl:value-of select="localization:getGTPString($language, 'XSL_GTEEDETAILS_TEXT_TYPE_VIEW_EDITED_DOCUMENT')"/></xsl:attribute>
					         <xsl:value-of select="localization:getGTPString($language, 'XSL_GTEEDETAILS_TEXT_TYPE_VIEW_EDITED_DOCUMENT')"/>
					        </a>)
			        	</xsl:with-param>
			        </xsl:call-template>
			     </xsl:if>
				<xsl:call-template name="input-field">
				   <xsl:with-param name="name">standby_rule_other</xsl:with-param>
				   <xsl:with-param name="maxsize">35</xsl:with-param>
				   <xsl:with-param name="readonly">Y</xsl:with-param>
				   <xsl:with-param name="required">Y</xsl:with-param>
				</xsl:call-template>
	    	</xsl:otherwise>
	    </xsl:choose>
	    
	    </xsl:with-param>
	   </xsl:call-template>
  </xsl:template>
 
 
		
		</xsl:stylesheet>
